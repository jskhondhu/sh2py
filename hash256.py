#from sh to py

#
# echo -n 'confidential data' | openssl dgst -sha256
# echo -n 'confidential data' | shasum -a256
#
# Its always nice to ask for the input
#

import hashlib
  
def encrypt_string(hash_string):
    sha_signature = \
        hashlib.sha256(hash_string.encode()).hexdigest()
    return sha_signature

hash_string = raw_input("Enter the string you want to SHA256 hash: ")
sha_signature = encrypt_string(hash_string)
print(sha_signature)

#3fef7ff0fc1660c6bd319b3a8109fcb9f81985eabcbbf8958869ef03d605a9eb